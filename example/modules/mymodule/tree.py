import inspect
import time

from NucleusApp.app import Manager


def out(tab, *text):
    print('\t' * tab, *text)


def field(tab, name, value=None, extra=''):
    line = name
    if value:
        line += ': ' + type(value).__name__ + ' = ' + str(value)

    line += ' ' + extra

    out(tab, line)


DATA_TYPES = (str, int, float, list, dict,)


def tree():
    def get_elements(tab, element):
        for attr_name in dir(element):
            if attr_name.startswith('_'):
                # Skip magic methods and private
                continue
            # Get attribute
            attr = getattr(element, attr_name)

            if attr in elements and attr is not None:
                out(tab, attr_name + ':', '(' + str(...) + ')', '[' + type(attr).__name__ + ']', str(attr))
                continue
            elements.append(attr)

            try:
                if isinstance(attr, (str, int, float, list, dict, bool)) or \
                        issubclass(attr.__class__, (str, int, float, list, dict, bool,)) or \
                        attr is None or \
                        type(attr).__name__ == 'property' or \
                        type(attr) in (str, int, float, list, dict, bool,):
                    field(tab, attr_name, attr)
                elif inspect.ismethod(attr):
                    out(tab, '.' + attr_name + '(self, *args, **kwargs)')
                elif inspect.isfunction(attr):
                    out(tab, attr_name + '(*args, **kwargs)')
                elif inspect.isbuiltin(attr):
                    continue
                else:
                    field(tab, attr_name, extra=type(attr).__name__)
                    get_elements(tab + 1, attr)
            except Exception as e:
                out(tab, attr_name, e.__class__.__name__ + ': ' + str(e))

    with Manager() as app:
        elements = [app]
        print('Inspect', app.name + ':')
        get_elements(1, app)


def on_ready():
    time.sleep(2)
    tree()

# with Manager() as application:
#     application.on_ready += on_ready
