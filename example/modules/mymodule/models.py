from playhouse.postgres_ext import *

from NucleusApp.middlewares.peewee_postgres.helper import BaseModel


class MyModel(BaseModel):
    test = TextField(default='')
