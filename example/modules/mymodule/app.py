from NucleusApp.modules.config import AppConfig


class Config(AppConfig):
    """
    Here you can setup application label
    and path and an action that will be triggered when application is ready for working.
    """
    label = 'MyModule'

    def ready(self):
        print(self.label, 'is ready!')
