import uuid

from NucleusApp.app import Manager
from .models import MyModel


def at_exit():
    print('Bye :)')


def application_is_ready():
    print('Application is ready!')

    # Create db record.
    MyModel.create(test=str(uuid.uuid4()))


# Get application instance
with Manager() as app:
    # Subscribe for exiting event
    # In particular for getting Application instance need to use `__call__` method.
    # NucleusApp Application was written as Singleton object
    app.on_exit += at_exit

    # Subscribe for app.ready event. It is triggered when application was configured
    # For example use other subscribing method `.subscribe()'
    # In previous example used builtin `__iadd__()` method. It called when usage `+=` expression
    app.on_ready.subscribe(application_is_ready)
