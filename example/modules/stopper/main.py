from NucleusApp.app import Manager

with Manager() as app:
    def on_ready():
        print('Application will be stopped after 5 seconds.')
        app.schedule.every(5).seconds.do(exit)


    app.on_ready += on_ready
