import os
import sys

sys.path.insert(0, os.path.abspath('..'))

from NucleusUtils.versions import Version

from NucleusApp.app import Application
from NucleusApp.chest import Chest


class ExampleApp(Application):
    """Example application"""

    # Here you need setup base information about application:
    # name, version, description
    # last can be written in class pydoc
    name = 'ExampleApp'
    version = Version(0, 1, 1, 'alpha', 2)

    # Needed set settings_module name.
    settings_module = 'settings'
    # And add to this list all application modules
    modules = [
        'modules.mymodule',
        'modules.stopper'
    ]
    middlewares = [
        'NucleusApp.middlewares.peewee_postgres.PostgresMiddleware'
    ]

    def prepare(self):
        # It will be called before loading modules but after load settings
        print('Prepare // dsmth')

    def run(self):
        # It will be called after loading modules and settings
        print('Do something here')


if __name__ == '__main__':
    # Init your application
    app = ExampleApp()

    # Can get base app info
    print(app.about())
    # Or base path
    print('Base dir:', app.path)

    # And then you can get application, settings and registry instance from Chest (This is singleton two)
    print(Chest().root['app'])

    # To load all submodules and settings call `.setup()` or `.start()` method.
    app.start()
