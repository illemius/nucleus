# TODO

## Engine
1. ~~Include scheduler~~
1. ~~Add event loop. mb replace old events // _For what?_~~
1. Refactor Middlewares engine
1. Refactor modules loader
1. Add post processing
1. Provide MongoDB // Migrate from Irisu_bot
1. Move Chest to NucleusUtils
1. Move import tools to NucleusUtils
1. Create new repository and write telegram module.

## Release
1. Add unit-tests
1. Add more examples
1. Write documentations
