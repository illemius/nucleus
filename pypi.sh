#!/usr/bin/env bash

function remove_build {
    rm -rf dist build NucleusApp.egg-info
}

function build {
    python setup.py sdist bdist_wheel
}

function rebuild {
    remove_build
    build
}

function upload {
    twine upload dist/*
}

function quit {
    exit
}

case $1 in
    build)
        build
    ;;
    rebuild)
        rebuild
    ;;
    upload)
        upload
    ;;
    clean)
        remove_build
    ;;
    *)
        echo "Usage: '$0 build|rebuild|upload|clean'"
        quit
    ;;
esac

