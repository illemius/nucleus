from NucleusUtils.versions import Version

VERSION = Version(0, 1, 1, 'beta', 5)
__version__ = VERSION.version
