class BaseMiddleware:
    name = None

    def __init__(self):
        pass

    def populate_module(self, appconfig):
        pass

    def start_application(self):
        pass
